import { StyleSheet, Text, View ,Image } from 'react-native'
import React from 'react'
import {createDrawerNavigator,useDrawerStatus, DrawerContentScrollView, DrawerItemList, useDrawerProgress} from '@react-navigation/drawer'
import Icon from 'react-native-vector-icons'
import PublicationsStack from "./PublicationsStack";
import AccountStack from "./AccountStack";
import PublishStack from "./PublishStack";
import InfoUser from "../components/Account/InfoUser";
import {
	ALTERNATIVE_COLOR,
	ALTERNATIVE_SECONDARY_COLOR_DARK,
	SECONDARY_COLOR,
} from "../utils/constants";

const Drawer = createDrawerNavigator();

const CustomDrawerContent=props=>{
    return(
        <DrawerContentScrollView style={{
            paddingVertical:30 }}>
            <View style={{marginLeft:20, marginVertical:40}}>
                {/* {InfoUser} */}
            </View>
            <DrawerItemList {...props}/>
        </DrawerContentScrollView>
    )
};



const DrawerScreenContainer =({children})=>{
    const progress= useDrawerProgress();

    return(
       
        <View style={{backgroundColor:white, flex:1,}}>
            {children}
            </View>
     )

};



const DrawerNavigator = () => {
  return (
    <Drawer.Navigator
    screenOptions={{
        headerShown:false,

                    drawerType:'slide',
                    drawerStyle:{
                        width:250,
                        backgroundColor: ALTERNATIVE_COLOR,
                        opacity:100000,
                    },
                    overlayColor: null,
                    sceneContainerStyle:{
                        backgroundColor:ALTERNATIVE_SECONDARY_COLOR_DARK,
                    },
                    drawerActiveTintColor: ALTERNATIVE_COLOR,
                    drawerInactiveTintColor: SECONDARY_COLOR,
                    drawerItemStyle: {backgroundColor: null},
                    drawerLabelStyle:{
                        fontWeight:'bold',
                    },
                }}
                drawerContent={props=> <CustomDrawerContent{...props}/>}>
                    
        <Drawer.Screen name='Home'
		component={PublicationsStack}
         options={{title:'Home',
         drawerIcon:(color)=><Icon name="home" 
          size={25} color={color}
          style={{marginRight:-20}}
         />}}>

             {props => (
                 <DrawerScreenContainer>
                     <HomeScreen {...props}/>
                 </DrawerScreenContainer>
             )}
         </Drawer.Screen>

         
         <Drawer.Screen name='Upload'
		 component={PublishStack}
         options={{title:'Upload',
		 drawerIcon:(color)=><Icon name="cloud-upload" 
		 
          size={25} color={color}
          style={{marginRight:-20}}
         />}}>

             {props => (
                 <DrawerScreenContainer>
                     <HomeScreen {...props}/>
                 </DrawerScreenContainer>
             )}
         </Drawer.Screen>

         <Drawer.Screen name='Chat'
         options={{title:'Chats',
         drawerIcon:(color)=><Icon name="chat" 
          size={25} color={color}
          style={{marginRight:-20}}
          
         />}}>

             {props => (
                 <DrawerScreenContainer>
                     <HomeScreen {...props}/>
                 </DrawerScreenContainer>
             )}
         </Drawer.Screen>



         <Drawer.Screen name='Setting'
		 component={AccountStack}
         options={{title:'Setting',
         drawerIcon:(color)=><Icon name="settings" 
          size={25} color={color}
          style={{marginRight:-20}}
         />}}>

             {props => (
                 <DrawerScreenContainer>
                     <HomeScreen {...props}/>
                 </DrawerScreenContainer>
             )}
         </Drawer.Screen>

         <Drawer.Screen name='Signout'
		 
         options={{title:'Signout',
         drawerIcon:(color)=><Icon name="logout" 
          size={25} color={color}
          style={{marginRight:-20}}
         />}}>

             {props => (
                 <DrawerScreenContainer>
                     <HomeScreen {...props}/>
                 </DrawerScreenContainer>
             )}
         </Drawer.Screen>


        
    </Drawer.Navigator>
  )
}
export default DrawerNavigator;