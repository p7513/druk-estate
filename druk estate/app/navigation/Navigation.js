import React,{useContext} from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { Icon } from "react-native-elements";
import {
	ALTERNATIVE_COLOR,
	ALTERNATIVE_SECONDARY_COLOR_DARK,
	SECONDARY_COLOR,
} from "../utils/constants";
import { createStackNavigator } from "@react-navigation/stack";
import PublicationsStack from "./PublicationsStack";
import AccountStack from "./AccountStack";
import PublishStack from "./PublishStack";
import OnboardingScreen from "../screens/OnboardingScreen";
import UserUploads from "../screens/UserUploads";
import Userpublication from "../screens/Userpublication";
import Edit from "../screens/EditUploads";
import Inbox from "../screens/Inbox";
import ChatScreen from "../screens/ChatScreen"
import { FirebaseContext } from "../context/firebase/FirebaseContext";
import UserGuest from "../screens/Account/UserGuest";
import ResetPasswordScreen from "../components/Account/ResetPassword";


const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

export default function Navigation() {
	const { isAuth, user } = useContext(FirebaseContext);
	if(user === null) return <Loading isVisible={true} text={'Loading...'} />
	return (
		<>
			<NavigationContainer>
			
				<Stack.Navigator
					presentation={"modal"}
					initialRouteName="Onboarding"
					screenOptions={{
						headerStyle: {
							backgroundColor: SECONDARY_COLOR,
						},
						headerTintColor: "#fff",
					}}
				>
					<Stack.Screen 
						name= "Onboarding"
						component={OnboardingScreen}
						options = {{headerShown: false}} 
						/>
					<Stack.Screen 
						name= "Chat"
						component={ChatScreen}
						 
						/>
					{isAuth ?  ( <Stack.Screen 
						name= "Inbox"
						component={Inbox}
						/>
					) : ( <Stack.Screen
						name="UserGuest"
						component={UserGuest}
						options = {{headerShown: false}}
						/>
				)}
					<Stack.Screen
						name="Home"
						component={HomeTab}
						options={{ headerShown: false }}
					/>
					<Stack.Screen 
						name="UserUploads"
						component={UserUploads}
						options ={{headerShown: false}}
						 />
					<Stack.Screen 
						name="Userpublication"
						component={Userpublication}
						options={({ route }) => ({
							title: String(route.params.data.title || 'Post').slice(0,16) + (String(route.params.data.title).length > 16 ? '...' :'') , 
				  headerShown:false
						})}
						/>
					<Stack.Screen
					name="EditUpload"
					component={Edit}
					options={{
						title: "Edit",
						headerShown:false
					}} />
					<Stack.Screen
					name="Reset"
					component={ResetPasswordScreen}
					options={{
						title: "Reset Your Password"
					}} />
					
					
					
					
				</Stack.Navigator>
			</NavigationContainer>
		</>
	);
}

const HomeTab = () => {
	return (
		<Tab.Navigator
		
			initialRouteName="Publications"
			tabBarOptions={{
				inactiveTintColor: "#646464",
				activeTintColor: ALTERNATIVE_COLOR,
				style: {
					paddingVertical: 5,
					marginBottom: 10,
					paddingHorizontal: "2.5%",
					
				},
				showLabel: false,
			}}
			screenOptions={({ route, navigation }) => ({
				tabBarIcon: ({ color }) => screenOptions(route, color, navigation),
			})}
		>
			<Tab.Screen
				name="Publications"
				component={PublicationsStack}
				options={{
					title: "Posts",
					headerShown:false
				}}
			/>
			
			<Tab.Screen
				name="Publish"
				component={PublishStack}
				options={{
					title: "Uploads",
					headerShown:false
				}}
			/>
			<Tab.Screen
				name="Account"
				component={AccountStack}
				options={{
					title: "Account",
					headerShown:false
				}}
			/>
		</Tab.Navigator>
	);
};

const screenOptions = (route, color, navigation) => {
	let iconName;
	switch (route.name) {
		case "Publications":
			iconName = "home-outline";
			break;
		
		case "Publish":
			iconName = "plus-circle";
			color = SECONDARY_COLOR;
			if (navigation.isFocused()) {
				color = ALTERNATIVE_SECONDARY_COLOR_DARK;
			}

			break;
		
		case "Account":
			iconName = "account-outline";
			break;
	}
	return (
		<Icon type="material-community" name={iconName} size={26} color={color} />
	);
};
