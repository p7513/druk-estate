import React, { useRef } from "react";
import {
	createStackNavigator,
} from "@react-navigation/stack";
import Account from "../screens/Account/Account";
import { SECONDARY_COLOR } from "../utils/constants";
import EditProfile from "../components/Account/AccountOptions/EditProfile";
import { StyleSheet } from "react-native";

const Stack = createStackNavigator();
export default function AccountStack() {
	const submitRef = useRef(null);
	
	return (
		<>
			<Stack.Navigator
				screenOptions={{
					headerTintColor: "#fff",
				}}

				presentation="modal"
			>
				<Stack.Screen
					name="Account"
					component={Account}
					
					options={{
					
						title: "                         Account",
						headerTintColor: "white",
						headerTransparent: true,
						headerShown:false
					}}
					
				/>

				<Stack.Screen
					name="EditProfile"
					options={{
						title: "EditProfile",
						headerShown:false
						
					}}
					children={() => <EditProfile ref={submitRef} />}

				/>
				
			</Stack.Navigator>
		</>
	);
}



const styles = StyleSheet.create({

	header: {
		backgroundColor: SECONDARY_COLOR,
	},
	
	btnContainer: {
		marginRight: 8,
	},
	titleBtn: {
		color: "#fff",
	},
});
