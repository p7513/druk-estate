import { useNavigation } from "@react-navigation/native";
import React, {  } from "react";
import { View, Text, StyleSheet } from "react-native";
import { Input } from "react-native-elements";

export default function FormPublish({title, description, setDescription, setTitle, price, setPrice, location, setLocation}) {

	const navigation = useNavigation();


	const priceConverter = (e) => {
		const rate = parseInt(e)
		if (rate > 0) {
      setPrice(rate);
    }
    if (!rate) {
      setPrice(0);
    }
	}
	return (
		<>
			<View>
				<Text style={{color:'white', fontSize:15, paddingLeft:45,paddingBottom:2}}>Price: </Text>
				<Input
					multiline={true}
					inputStyle={[styles.input, ]}
					containerStyle={{ paddingHorizontal: 0 }}
					inputContainerStyle={styles.divider}
					errorStyle={{ display: "none" }}
					keyboardType='numeric'
					placeholder="Price"
					maxLength={10}
					value={price.toString()}
					onChangeText={priceConverter}
				/>
				<Text style={{color:'white', fontSize:15, paddingLeft:45,paddingBottom:2}}>Location: </Text>
				<Input
					maxLength={50}
					inputStyle={styles.input}
					containerStyle={{ paddingHorizontal: 0 }}
					inputContainerStyle={styles.divider}
					errorStyle={{ display: "none" }}
					// placeholder="Location"
					value={location}
					onChangeText={(e) => setLocation(e)}
				/>
				<Text style={{color:'white', fontSize:15, paddingLeft:45,paddingBottom:2}}>House Type: </Text>
				<Input
					maxLength={50}
					inputStyle={styles.input}
					containerStyle={{ paddingHorizontal: 0 }}
					inputContainerStyle={styles.divider}
					errorStyle={{ display: "none" }}
					// placeholder="House Type"
					value={title} 
					onChangeText={(e) => setTitle(e)}
				/>
				<Text style={{color:'white', fontSize:15, paddingLeft:45,
			paddingBottom:2}}>Description: </Text>
				<Input
					multiline={true}
					inputStyle={[styles.input,]}
					containerStyle={{ paddingHorizontal: 0 }}
					inputContainerStyle={styles.divider}
					errorStyle={{ display: "none" }}
					// placeholder="Description"
					maxLength={400}
					value={description} 
					onChangeText={(e) => setDescription(e)}
				/>
				<Text style={[styles.maximo, styles.divider]}>
					Maximum 400 characters
				</Text>
				
				
			</View>
		</>
	);
}

const styles = StyleSheet.create({
	input: {
	
		margin:30,
		marginBottom:10,
		marginTop:0,
		borderWidth:2,
		borderColor:'white',
		borderRadius:15,
		padding:14,
		color:'white'
		
	},
	divider: {
		borderBottomWidth: 0,
		borderColor: "#dbdbdb",
	},
	maximo: {
		
		margin: 40,
		marginTop: 15,
		fontSize: 13,
		color: "#e3e3e3",
		
	},
});
