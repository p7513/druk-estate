import React, { useContext, useState } from "react";
import { View, Text, StyleSheet } from "react-native";
import { Avatar } from "react-native-elements";
import { FirebaseContext } from "../../context/firebase/FirebaseContext";
import { SECONDARY_COLOR } from "../../utils/constants";
import * as Permissions from "expo-permissions";
import * as ImagePicker from "expo-image-picker";
import Toast from "react-native-toast-message";
import Loading from "../Loading";
import {LinearGradient} from 'expo-linear-gradient'
export default function InfoUser() {
	const { user, firebase, setReloadUser, userInfo } = useContext(FirebaseContext);
	const [loading, setLoading] = useState(false);
	const [loadingText] = useState("Profile Updating");
	return (
		<LinearGradient colors={['#4ca4fc','#b375e6']} style={{borderBottomLeftRadius:50}}>
			<View style={styles.container}>
				
				<Avatar
					rounded
					size="large"
					containerStyle={styles.userInfoAvatar}
					activeOpacity={0.7}		
					icon={{ name: "user", type: "font-awesome" }}
					source={{ uri: user.photoURL }}
				/>
				<View style={styles.data}>
					<Text style={{fontSize:20,fontWeight:'bold', color:'#5b5c5b'}}>{user.displayName || "Name"} </Text>
					<Text style={{fontSize:12,fontWeight:'bold', color:'#dee0e0'}}>{user.email}</Text>
					
				</View>
				
				<Loading isVisible={loading} text={loadingText} />
			</View>
			</LinearGradient>
			
	);
}
const styles = StyleSheet.create({
	container: {
		// backgroundColor: SECONDARY_COLOR,
		paddingTop: 30,
		paddingBottom: 30,

		alignItems: "center",
		justifyContent: "center",
		
	},
	userInfoAvatar: {
		backgroundColor: "#7e7e7e",
	},
	data: {
		alignItems: "center",
		paddingVertical: 10,
	},
});
