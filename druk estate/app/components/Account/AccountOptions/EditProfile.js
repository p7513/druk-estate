import { useFormik } from "formik";
import React, {
	forwardRef,
	useContext,
	useImperativeHandle,
	useState,
} from "react";
import { StyleSheet, ScrollView, View, Text, TouchableOpacity  } from "react-native";
import { Input,} from "react-native-elements";
import { FirebaseContext } from "../../../context/firebase/FirebaseContext";
import HeaderTitle from "../../HeaderTitle";
import * as yup from "yup";
import { useNavigation } from "@react-navigation/native";
import Toast from "react-native-toast-message";
import Loading from "../../Loading";
import { SECONDARY_COLOR } from "../../../utils/constants";
import { Button, Avatar } from "react-native-elements";
import { ALTERNATIVE_COLOR } from "../../../utils/constants";
import { Ionicons } from '@expo/vector-icons';
import * as Permissions from "expo-permissions";
import * as ImagePicker from "expo-image-picker";
import {LinearGradient} from 'expo-linear-gradient'

const EditProfile = forwardRef((props, ref) => {
	const { user, firebase, setReloadUser, userInfo, db } = useContext(
		FirebaseContext,
	);

	const [loading, setLoading] = useState(false);
	const navigation = useNavigation();
	const [loadingText] = useState("Profile Updating");
	const changeAvatar = async () => {
		const resultPermision = await Permissions.askAsync(Permissions.CAMERA);
		const resultPermisionCamera = resultPermision.permissions.camera.status;

		if (resultPermisionCamera === "denied") {
			Toast.show({
				type: "info",
				autoHide: true,
				text1: "Permission Denied to access Gallery",
				visibilityTime: 2500,
			});
		} else {
			const result = await ImagePicker.launchImageLibraryAsync({
				allowsEditing: true,
				aspect: [4, 3],
			});

			if (!result.cancelled) {
				uploadImage(result.uri)
					.then((res) => {
						updatePhotoURL();
					})
					.catch(() => {
						Toast.show({
							type: "error",
							autoHide: true,
							text1:
								"Unable to upload photo",
							visibilityTime: 2500,
						});
					});
			}
		}
	};

	const uploadImage = async (uri) => {
		setLoading(true);

		const response = await fetch(uri);
		const blob = await response.blob();

		const ref = firebase.storage().ref().child(`avatar/${user.uid}.jpg`);
		return ref.put(blob);
	};
	const updatePhotoURL = () => {
		firebase
			.storage()
			.ref(`avatar/${user.uid}.jpg`)
			.getDownloadURL()
			.then(async (URL) => {
				const update = {
					photoURL: URL,
				};
				await firebase.auth().currentUser.updateProfile(update);
				setLoading(false);
				setReloadUser(true);
			})
			.catch(() => {
				Toast.show({
					type: "error",
					autoHide: true,
					text1:
						"Unable to upload photo",
					visibilityTime: 2500,
				});
			});
	};


	const handleSubmit = () => {
		formik.handleSubmit();
	};

	useImperativeHandle(
		ref,
		() => ({
			handleSubmit,
		}),
		[],
	);
	const formik = useFormik({
		initialValues: {
			email: user.email,
			displayName: user.displayName,
			
		},
		validationSchema: yup.object().shape({
			email: yup
				.string()
				.required("An email is required")
				.email("email invalid"),
			displayName: yup
				.string()
				.required("Please state a name")
				.min(2, "Your Name should contain atleast 2 characters")
				.max(50, "Should not exceed more than 50 characters"),		
		}),
		onSubmit: async (data) => {
			const { email, displayName, } = data;
			setLoading(true);
			const user = firebase.auth().currentUser;
			let userInfo = db.collection("users").doc(user.uid);

			try {
				await user.updateEmail(email);
				await user.updateProfile({
					displayName,				
				});
				
				
				Toast.show({
					type: "success",
					position: "top",
					text1: "User successfully added .",
					visibilityTime: 2500,
					autoHide: true,
				});
				setReloadUser(true);

				navigation.navigate("Account");
			} catch (error) {
				setLoading(false);
				let errorMessage = "User creation failed";
				let errorMessage2 = "Unable to create user.";

				console.log(error.code);
				if (error.code === "auth/requires-recent-login") {
					errorMessage = "Requires reauthentication";
					errorMessage2 = "Please login once again";
				}

				if (error.code === "auth/email-already-in-use") {
					errorMessage = "email already in use";
					errorMessage2 =
						"please use a valid new email";
				}
				Toast.show({
					type: "error",
					position: "top",
					text1: errorMessage,
					text2: errorMessage2,
					visibilityTime: 2500,
					autoHide: true,
				});
			}
			setReloadUser(true);
		},
	});

	

	return (
		<>
		<LinearGradient colors={['#4ca4fc','#fa64ca']} style={{flex:1}}>
			<ScrollView>
			
			<View style={styles.header}>
				
				<Ionicons name="arrow-back-sharp" size={30} color="white" onPress={navigation.goBack}/>
				<View style={{margin:100,marginBottom:0,marginTop:0}}>
				<Text style={{fontSize:20, fontWeight:'bold',color:'white'}}>Edit Profile</Text>
				</View>			
				</View>
				
				<Avatar
					rounded
					size="large"
					style={styles.userInfoAvatar}
					activeOpacity={0.7}
					
					onPress={changeAvatar}
					icon={{ name: "user", type: "font-awesome" }}
					source={{ uri: user.photoURL }}
				><Avatar.Accessory  size={50} style={{backgroundColor:'#8c76db'}}/></Avatar>
				<Loading isVisible={loading} text={loadingText} />
				
				<View style={styles.mail}>
					
					<Input
					style={styles.in}
						
						inputContainerStyle={styles.divider}
						containerStyle={styles.inputContainer}
						errorStyle={{ display: "none" }}
						inputStyle={styles.input}

						returnKeyType="done"
						value={formik.values.displayName}
						onBlur={formik.handleBlur("displayName")}
						onChangeText={formik.handleChange("displayName")}
						errorMessage={formik.errors.displayName}
					/>
					
				</View>

				<View style={styles.mail}>		
					<Input
					style={styles.in}
						

						inputContainerStyle={styles.divider}
						containerStyle={styles.inputContainer}
						errorStyle={{ display: "none" }}
						inputStyle={styles.input}

						
						textContentType="emailAddress"
						keyboardType="email-address"
						autoCompleteType="email"
						autoCapitalize="none"
						returnKeyType="done"
						value={formik.values.email}
						onBlur={formik.handleBlur("email")}
						onChangeText={formik.handleChange("email")}
						errorMessage={formik.errors.email}
					/>
				</View>

				
			
					<TouchableOpacity onPress={handleSubmit} style={styles.btn}>
				<Text style={{fontSize:20,color:'white', fontWeight:'bold'}}>Update</Text>
				</TouchableOpacity>
				
				<Loading isVisible={loading} text="Loading" />
				
			</ScrollView>
			</LinearGradient>
		</>
	);
});
const styles = StyleSheet.create({

	// headerTitle: {
	// 	paddingTop: 0,
	// },
	input: {
		backgroundColor: "#fff",
		paddingHorizontal: 14,
		paddingVertical: 20,
	},
	// divider: {
	// 	borderBottomWidth: 0,
	// 	borderColor: "#d7dbd9",
	// },
	inputContainer: {
		marginTop:20,
		position: "relative",
		
		
		
	},
	in:{
backgroundColor:'transparent',
borderWidth:2,
borderColor:'white',
borderRadius:20,
color:'white',
paddingLeft:25
	},
	// info: {
	// 	backgroundColor: SECONDARY_COLOR,
	// 	height: "100%",
	// 	width: "100%",
	// 	position: "absolute",
	// },
	
		header:{
			flexDirection:'row',
			paddingLeft:15,
			height:60,
			alignItems:'center',
			// backgroundColor:'#38c1bd'
		},
		btn:{
			justifyContent:'center',
			alignItems:'center',
			height:50,
			backgroundColor:'#8c76db',
			borderRadius:60,
			margin:80,
			marginTop:50,
			marginBottom:30,
			elevation:10
	
		},
		userInfoAvatar:{
			height:300,
			margin:45,
			marginTop:20,
			marginBottom:15,
			backgroundColor:'#d7dbd9',
			borderRadius:50,
			elevation:10,
			shadowColor:"#38c1bd"
			
			
		},
		mail:{
		
			height:90,
			justifyContent:'center',
			margin:30,
			marginBottom:0,
			marginTop:0
			
		}
	
});

export default EditProfile;
