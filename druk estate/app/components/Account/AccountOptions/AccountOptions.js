import React from "react";
import { View, Text, TouchableOpacity, Dimensions} from "react-native";
import { map } from "lodash";
import { Icon, ListItem } from "react-native-elements";
import {
	ALTERNATIVE_COLOR,
	ALTERNATIVE_SECONDARY_COLOR,
	GRAY_COLOR,
} from "../../../utils/constants";
import { StyleSheet } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { FontAwesome5, MaterialCommunityIcons, Ionicons} from '@expo/vector-icons';

const {width,height}=Dimensions.get('window');

export default function AccountOptions() {
	const navigation = useNavigation();
	return (
		<>
			<View style={styles.container}>
				<View  >
				
				<TouchableOpacity onPress={() => navigation.navigate("EditProfile")} style={styles.edit}>
				
				<Text style={{fontWeight:'bold',color:'grey'}}><FontAwesome5 name="user-edit" size={20} color="grey" />  Edit Profile</Text>
				</TouchableOpacity>
				</View>

				<View  >
					<TouchableOpacity onPress={() => navigation.navigate("Publish")} style={styles.upload} >
				<Text style={{fontWeight:'bold',color:'grey'}}><MaterialCommunityIcons name="upload" size={24} color="grey" />  Upload</Text>
				</TouchableOpacity>
				</View>

				<View >
				<TouchableOpacity onPress={() => navigation.navigate("UserUploads")}  style={styles.uploads}>
				<Text style={{fontWeight:'bold',color:'grey'}}><MaterialCommunityIcons name="file-upload" size={20} color="grey" />  Uploads</Text>
				</TouchableOpacity>
				</View>

				<View >
				<TouchableOpacity onPress={() => navigation.navigate("Inbox")} style={styles.chat}>
				<Text style={{fontWeight:'bold',color:'grey'}}><Ionicons name="chatbox-ellipses" size={20} color="grey" />  Chats</Text>
				</TouchableOpacity>
				</View>
				{/* <ListItem
					bottomDivider
					onPress={() => navigation.navigate("EditProfile")}
				>
					<Icon
						name="account"
						type="material-community"
						color={GRAY_COLOR}
					/>
					<ListItem.Content>
						<ListItem.Title>Edit Profile</ListItem.Title>
					</ListItem.Content>
					<ListItem.Chevron type="font-awesome" name="chevron-right" />
				</ListItem>
		

			<View style={styles.container1}>
				<ListItem bottomDivider
					onPress={() => navigation.navigate("UserUploads")}
					>
					<Icon
						name={"application"}
						type={"material-community"}
						color={ALTERNATIVE_SECONDARY_COLOR}
					/>
					<ListItem.Content>
						<ListItem.Title>Upload</ListItem.Title>
					</ListItem.Content>
					<ListItem.Chevron type="font-awesome" name="chevron-right" />
				</ListItem>
				</View>

				<View style={styles.container2}>
				<ListItem bottomDivider
					onPress={() => navigation.navigate("UserUploads")}
					>
					<Icon
						name={"application"}
						type={"material-community"}
						color={ALTERNATIVE_SECONDARY_COLOR}
					/>
					<ListItem.Content>
						<ListItem.Title>Uploads</ListItem.Title>
					</ListItem.Content>
					<ListItem.Chevron type="font-awesome" name="chevron-right" />
				</ListItem>
				</View>

				<View>
				<ListItem
					bottomDivider
					onPress={() => navigation.navigate("Inbox")}
				>
					<Icon
						name={"message"}
						type={"material-community"}
						color={ALTERNATIVE_COLOR}
					/>
					<ListItem.Content>
						<ListItem.Title>Inbox</ListItem.Title>
					</ListItem.Content>
					<ListItem.Chevron type="font-awesome" name="chevron-right" />
				</ListItem>
			</View> */}
			</View>
		</>
	);
}

const styles = StyleSheet.create({
	container: {
		flex:1,
		// backgroundColor:'white',
		margin:width<380 ? 15:20,
		// borderRadius:10,
		// elevation:10,
		// backgroundColor:'red',
		paddingLeft:width<400? 0:20
		
	},

	edit:{
		height:height<380 ? 140:160,
		backgroundColor:'#e3e8e5',
		width:width<380 ? 140:160,
		justifyContent:'center',
		alignItems:'center',
		borderRadius:30,
		margin:5,
		elevation:10,
		// marginTop:0
		
		
		
	},
	upload:{
		height:160,
		backgroundColor:'#e3e8e5',
		width:width<380 ? 140:160,
		justifyContent:'center',
		alignItems:'center',
		borderRadius:30,
		marginTop:10,
		marginLeft:5,
		elevation:10,
	},

	uploads:{
		height:160,
		backgroundColor:'#e3e8e5',
		width:width<380 ? 140:160,
		justifyContent:'center',
		alignItems:'center',
		marginLeft:180,
		marginTop:-335,
		borderRadius:30,
		margin:10,
		elevation:10,
	},

	chat:{
		height:160,
		backgroundColor:'#e3e8e5',
		width:width<380 ? 140:160,
		justifyContent:'center',
		alignItems:'center',
		marginLeft:180,
		borderRadius:30,
		marginRight:10,
		marginTop:5,
		elevation:10,
		marginTop:-160,
	},
});
