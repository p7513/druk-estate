import React, {useState} from 'react'
import { StyleSheet, View } from "react-native";
import { Text, Input, Button, Icon } from "react-native-elements";
import { resetPassword } from "../../api/auth-api";
import { emailValidator } from '../emailValidator';
import FormContainer from '../FormContainer';
import HeaderTitle from '../HeaderTitle';
import { ALTERNATIVE_COLOR, ERROR_COLOR, PRIMARY_COLOR } from "../../utils/constants";

export default function ResetPasswordScreen({navigation}){
    const [email,setEmail] = useState({value:"", error: ""})
    const [loading, setLoading] = useState();

    const onSubmitPressed = async() => {
        const emailError = emailValidator(email.value); 
       
        if(emailError){
            setEmail({ ...email,error:emailError });
        }
        setLoading(true)
        const response = await resetPassword({
            email: email.value
        });
        if (response.error) {
            alert(response.error);
        }
        else{
           Alert.alert("Password reset link sent!")
        }
        setLoading(false)
    }


    return(
        <>
        <FormContainer>
            <HeaderTitle text={"Reset your Password"} />
            <View style={styles.form}>
					<Input
						placeholder="Email"
						rightIcon={{
							type: "material-community",
							name: "email-outline",
							color: "#8f8f8f",
							iconStyle: styles.icon,
						}}
						textContentType="emailAddress"
						keyboardType="email-address"
						autoCompleteType="email"
						autoCapitalize="none"
						
						returnKeyType="done"
						inputStyle={styles.input}
						inputContainerStyle={styles.inputContainer}
						labelStyle={styles.label}
						placeholderTextColor="#686868"
						value={email.value}
						onChangeText={(text) => setEmail({value:text,error:""})}
						errorStyle={{ color: ERROR_COLOR }}
					/>
                    </View>
                   
        </FormContainer>
        <Button 
                        title='Send Instructions'
                        loading = {loading} 
                        mode = "contained" 
                        onPress={onSubmitPressed} />
    
    </>
    )

}
const styles = StyleSheet.create({
	form: {
		flex: 1,
		justifyContent: "flex-start",
		marginTop: 20,
	},
	inputContainer: {
		borderBottomColor: "#8f8f8f",
		borderBottomWidth: 0.5,
		height: 28,
		paddingBottom: 10,
		marginTop: 10,
	},
	input: {
		fontSize: 16,
		fontWeight: "300",
	},
	icon: {
		fontSize: 20,
	},
	submit: {
		backgroundColor: PRIMARY_COLOR,
	},
	cambiar: {
		flexDirection: "row",
		paddingVertical: 10,
		marginTop: 20,
		justifyContent: "center",
	},
	alternativeTxt: {
		color: ALTERNATIVE_COLOR,
		fontWeight: "bold",
	},
	error: {
		color: ERROR_COLOR
	}
});
