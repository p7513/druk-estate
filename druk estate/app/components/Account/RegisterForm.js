import React, { useContext, useState } from "react";
import { StyleSheet, View } from "react-native";
import { Text, Input, Button, Icon } from "react-native-elements";
import Toast from "react-native-toast-message";

import { useFormik } from "formik";
import * as yup from "yup";

import {
	ALTERNATIVE_COLOR,
	ERROR_COLOR,
	PRIMARY_COLOR,
} from "../../utils/constants";
import FormContainer from "../FormContainer";
import HeaderTitle from "../HeaderTitle";
import { FirebaseContext } from "../../context/firebase/FirebaseContext";

export default function RegisterForm({ setLoginComponent }) {
	const [hiddenPass, setHiddenPass] = useState(true);
	const [loading, setLoading] = useState(false);
	const [emailError, setEmailError] = useState("");
	const { firebase, setReloadUser, db } = useContext(FirebaseContext);
	const formik = useFormik({
		initialValues: {
			Name: "",
			phonenumber:"",
			email: "",
			password: "",
			confirmpassword:""
		},
		validationSchema: yup.object().shape({
			Name: yup
				.string()
				.required("Your Name is required")
				.max(50, ""),
			email: yup
				.string()
				.required("Please put your email id")
				.email("Your Email is invalid"),
			password: yup
				.string()
				.required("Please enter a strong Password")
				.min(5, "atleast 5 charater is required "),
			phonenumber: yup
				.string()
				.required("Enter your Phone Number")
				.min(8,"atleast 8 numbers should be present"),
			confirmpassword: yup
				.string()
				.required("Please confirm your password")
				.oneOf([yup.ref('password')], 'Passwords does not match')

		}),
		onSubmit: async (data) => {
			setLoading(true);
			try {
				const newUser = await firebase
					.auth()
					.createUserWithEmailAndPassword(data.email, data.password);
				await newUser.user.updateProfile({
					displayName: data.Name,
				});
				await db.collection("users")
					.doc(newUser.user.uid)
					.set({
						displayName: data.Name,
						phonenumber: data.phonenumber,
						email: data.email,
						
											
					})
				
				setReloadUser(true);

				Toast.show({
					type: "success",
					position: "top",
					text1: "User registerd succesfully",
					visibilityTime: 2500,
					autoHide: true,
				});
			} catch (error) {
				setEmailError("Invalid Email");
				setLoading(false);
			}
		},
	});
	return (
		<>
			<FormContainer>
				<HeaderTitle text="Create Account" />
				<View style={styles.form}>
					<Input
						placeholder="Name"
						rightIcon={{
							type: "material-community",
							name: "account-outline",
							color: "#8f8f8f",
							iconStyle: styles.icon,
						}}
						errorMessage={
							formik.touched.Name ? formik.errors.Name : ""
						}
						returnKeyType="done"
						inputStyle={styles.input}
						inputContainerStyle={styles.inputContainer}
						placeholderTextColor="#686868"
						value={formik.values.Name}
						onBlur={formik.handleBlur("Name")}
						onChangeText={formik.handleChange("Name")}
						errorStyle={{ color: ERROR_COLOR }}
					/>
					<Input
						placeholder="Phone Number"
						rightIcon={{
							type: "material-community",
							name: "phone-outline",
							color: "#8f8f8f",
							iconStyle: styles.icon,
						}}
						errorMessage={
							formik.touched.phonenumber ? formik.errors.phonenumber : ""
						}
						returnKeyType="done"
						keyboardType="numeric"
						maxLength={8}
						inputStyle={styles.input}
						inputContainerStyle={styles.inputContainer}
						placeholderTextColor="#686868"
						value={formik.values.phonenumber}
						onBlur={formik.handleBlur("phonenumber")}
						onChangeText={formik.handleChange("phonenumber")}
						errorStyle={{ color: ERROR_COLOR }}
					/>
					<Input
						placeholder="Email"
						rightIcon={{
							type: "material-community",
							name: "email-outline",
							color: "#8f8f8f",
							iconStyle: styles.icon,
						}}
						errorMessage={
							(formik.touched.email ? formik.errors.email : "") ||
							emailError
						}
						textContentType="emailAddress"
						keyboardType="email-address"
						autoCompleteType="email"
						autoCapitalize="none"
						returnKeyType="done"
						inputStyle={styles.input}
						inputContainerStyle={styles.inputContainer}
						placeholderTextColor="#686868"
						value={formik.values.email}
						onBlur={formik.handleBlur("email")}
						onChangeText={formik.handleChange("email")}
						onChange={() => setEmailError("")}
						errorStyle={{ color: ERROR_COLOR }}
					/>

					<Input
						placeholder="Password"
						rightIcon={
							<Icon
								type="material-community"
								name={hiddenPass ? "eye-outline" : "eye"}
								color="#8f8f8f"
								iconStyle={styles.icon}
								onPress={() => setHiddenPass(!hiddenPass)}
							/>
						}
						errorMessage={
							formik.touched.password ? formik.errors.password : ""
						}
						secureTextEntry={hiddenPass}
						returnKeyType="done"
						inputStyle={styles.input}
						inputContainerStyle={styles.inputContainer}
						placeholderTextColor="#686868"
						value={formik.values.password}
						onChangeText={formik.handleChange("password")}
						onBlur={formik.handleBlur("password")}
						errorStyle={{ color: ERROR_COLOR }}
					/>
					<Input
						placeholder="Confirm Password"
						rightIcon={
							<Icon
								type="material-community"
								name={hiddenPass ? "eye-outline" : "eye"}
								color="#8f8f8f"
								iconStyle={styles.icon}
								onPress={() => setHiddenPass(!hiddenPass)}
							/>
						}
						errorMessage={
							formik.touched.confirmpassword ? formik.errors.confirmpassword : ""
						}
						secureTextEntry={hiddenPass}
						returnKeyType="done"
						inputStyle={styles.input}
						inputContainerStyle={styles.inputContainer}
						placeholderTextColor="#686868"
						value={formik.values.confirmpassword}
						onChangeText={formik.handleChange("confirmpassword")}
						onBlur={formik.handleBlur("confirmpassword")}
						errorStyle={{ color: ERROR_COLOR }}
					/>
				</View>
			</FormContainer>

			<Button
				buttonStyle={styles.submit}
				title="Register"
				onPress={() => formik.handleSubmit()}
				loading={loading}
				disabled={loading}
			/>
			<View style={styles.cambiar}>
				<Text>Already have an account? </Text>
				<Text
					style={[styles.alternativeTxt, { fontWeight: "bold" }]}
					onPress={() => setLoginComponent(true)}
				>
					SignIn
				</Text>
			</View>
		</>
	);
}

const styles = StyleSheet.create({
	form: {
		flex: 1,
		justifyContent: "flex-start",
		marginTop: 20,
	},
	inputContainer: {
		borderBottomColor: "#8f8f8f",
		borderBottomWidth: 0.5,
		height: 28,
		paddingBottom: 10,
		marginTop: 10,
	},
	input: {
		fontSize: 16,
		fontWeight: "300",
	},
	icon: {
		fontSize: 20,
	},
	submit: {
		backgroundColor: PRIMARY_COLOR,
	},
	cambiar: {
		flexDirection: "row",
		paddingVertical: 10,
		marginTop: 20,
		justifyContent: "center",
	},
	alternativeTxt: {
		color: ALTERNATIVE_COLOR,
		fontWeight: "bold",
	},
});
