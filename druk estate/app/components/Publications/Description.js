import React, { useContext, useEffect, useState } from "react";
import { StyleSheet } from "react-native";
import { View, Text } from "react-native";
import { Card } from "react-native-elements";
import { FirebaseContext } from "../../context/firebase/FirebaseContext";
import {
	ALTERNATIVE_SECONDARY_COLOR_DARK,
	SECONDARY_COLOR,
} from "../../utils/constants";
import { MaterialIcons } from '@expo/vector-icons';
import { ScrollView } from "react-native-gesture-handler";

export default function Description({ data, localDate }) {
	const {
		title,
		price,
		location,
		description,
		createdBy,
  } = data;
  const [userNumber, setUserNumber] = useState('')

	const { db } = useContext(FirebaseContext);

	useEffect(() => {
		db.collection("users")
			.doc(createdBy)
			.onSnapshot((snapshot) => {
				setUserNumber(snapshot.data());
			});
  }, []);
	return (
		<View style={styles.container}>
			

			<View>
				<Text style={styles.price}>Nu {price || "0"}</Text>	
			</View>
			<View style={styles.info}>
			<MaterialIcons name="location-on" size={18} color="#4ca4fc" />
			<Text style={{fontWeight:'bold',color:'#4ca4fc',fontSize:13,}}>{location}</Text>
			</View>
			<Text style={{fontSize:13,color:'#c7c9c8',paddingTop:5}}>Posted by: {createdBy}</Text>
			<Text style={styles.title}>{title}</Text>
				
			<Card.Divider />
		<View style={{height:260}}>
			<ScrollView>
			<Text style={{ paddingBottom: 20, fontSize:16,color:'black' }}>{description}</Text>
			</ScrollView>
		</View>
		</View>
	);
}
const styles = StyleSheet.create({
	container: {
		// backgroundColor: "grey",
		// paddingHorizontal: "4%",
		// paddingVertical: 10,
		flex:1,
		
	 },
	title: {
		color: '#c7c9c8',
		fontSize: 14,
		fontWeight: "bold",
		textTransform: "uppercase",
		paddingBottom: 8,
		paddingTop:6
	},
	info: {
		flexDirection: "row",
		alignItems:'center',
		paddingTop:15
	},
	price: {
		color: 'black',
		fontSize: 25,
		fontWeight: "bold",
	},
	// date: {
	// 	color: "#444",
	// 	fontSize: 12,
	// },
});
