import React from "react";
import dayjs from "dayjs";
import "dayjs/locale/es";
import {
	Text,
	StyleSheet,
	TouchableWithoutFeedback, Dimensions, View
} from "react-native";
import { ALTERNATIVE_SECONDARY_COLOR_DARK, SECONDARY_COLOR } from "../../utils/constants";

import { Card } from "react-native-elements";
import timezone from "dayjs/plugin/timezone";
import utc from "dayjs/plugin/utc";
import { MaterialIcons } from '@expo/vector-icons';

const {width}=Dimensions.get('screen');
const cardWidth= width/2-30;

dayjs.extend(utc);
dayjs.extend(timezone);
export default function CardComponent({ data, navigation }) {
	const { createdAt, images, location, title, price } = data;

		return (
		<TouchableWithoutFeedback
			onPress={() => navigation.navigate("Publication", { data })}
			style={styles.but}
		>
			<View style={styles.card}>
			<Card >
				{images.length > 0 ? (
					<Card.Image source={{ uri: images[0] }} style={{height:100,borderRadius:10,}}/>
				) : (
					<NoImage/>
				)}
			
				<View style={{flexDirection:'row', marginTop:5}}>
				<MaterialIcons name="location-on" size={18} color="#4ca4fc" />
				<Text style={{fontSize:12,fontWeight:'bold',color:'#4ca4fc'}}>
					{location}
				</Text>
				</View>

				<Text style={{fontSize:13,fontWeight:'bold'}}>Nu {price || 0}</Text>
				
			</Card>
			</View>
		</TouchableWithoutFeedback>
	);
}

const NoImage = () => (
	<Card.Image style={{height:100, justifyContent:'center',alignItems:'center',borderRadius:10}}>
		<Text style={{ color: "#fff",fontSize:10,fontWeight:'bold' }}>No photos to display.</Text>
	</Card.Image>
);

const styles = StyleSheet.create({
	
	card:{
		
		backgroundColor:'white',
		height:205,
        width: cardWidth,
        marginLeft:15,
        marginTop:25,
        borderRadius: 15,
        elevation:13,
		
	}
	
});
