import React, { useContext } from "react";
import { View, TouchableOpacity,Text, StyleSheet } from "react-native";
import {  Button } from "react-native-elements";
import InfoUser from "../../components/Account/InfoUser";
import { FirebaseContext } from "../../context/firebase/FirebaseContext";
import { PRIMARY_COLOR } from "../../utils/constants";
import AccountOptions from "../../components/Account/AccountOptions/";
// import {LinearGradient} from 'expo-linear-gradient'

export default function UserLogged() {
	const { firebase } = useContext(FirebaseContext);

	const EndSession = async () => {
		await firebase.auth().signOut();
	};

	return (
	
		<View style={{ flex: 1 }}>
		
			<View style={styles.header}>
				<Text style={{fontSize:20, fontWeight:'bold',color:'white'}}>Account</Text>
			</View>
			
			<InfoUser />
			<AccountOptions />
				<TouchableOpacity onPress={() => EndSession()} style={styles.btn}>
				<Text style={{fontSize:20,color:'white', fontWeight:'bold'}}>Log Out</Text>
				</TouchableOpacity>
				
		</View>
		
	);
}
const styles=StyleSheet.create({

	btn:{
		justifyContent:'center',
		alignItems:'center',
		height:45,
		backgroundColor:'#8c76db',
		borderRadius:60,
		margin:80,
		marginTop:0,
		marginBottom:30,
		elevation:10

	},
	header:{
		justifyContent:'center',
		paddingTop:30,
		height:60,
		alignItems:'center',
		backgroundColor:'#4ca4fc',
	},
})
