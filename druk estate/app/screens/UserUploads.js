import React, { useCallback, useContext, useState } from "react";
import { StyleSheet } from "react-native";
import { View, Text, FlatList, TouchableOpacity } from "react-native";
import Card from '../components/Publications/UserCard'
import { FirebaseContext } from "../context/firebase/FirebaseContext";
import { useFocusEffect, useNavigation } from "@react-navigation/native";
import {
	LIMIT_PUBLICATIONS,
} from "../utils/constants";

import { ActivityIndicator } from "react-native";
import Userpublication from "./Userpublication";
import {LinearGradient} from 'expo-linear-gradient'
import { Ionicons } from '@expo/vector-icons';
import { ScrollView } from "react-native-gesture-handler";

export default function UserUpload() {
	const { user, db } = useContext(FirebaseContext);
	const [totalPublications, setTotalPublications] = useState(0);
	const [pointer, setPointer] = useState(null);
	const [posts, setPosts] = useState([]);
	const [loading, setLoading] = useState(false);
	const navigation = useNavigation()
	useFocusEffect(
		useCallback(() => {
			db.collection("posts")
				 .where("createdBy", "==", user.displayName)
				.get()
				.then((snap) => {
					setTotalPublications(snap.size);
				});

			const resultPublications = [];

			db.collection("posts")
				.orderBy("createdAt", "desc")
				.where("createdBy", "==", user.displayName)
				.limit(LIMIT_PUBLICATIONS)
				.get()
				.then((snap) => {
					
					setPointer(snap.docs[snap.docs.length - 1]);
					snap.forEach((doc) => {
						resultPublications.push({ id: doc.id, ...doc.data() });
					});
					setPosts(resultPublications);
		});
		}, []),
	);

	const handleMore = () => {
		const resultPublications = [];
		if (posts.length < totalPublications) setLoading(true);
		db.collection("posts")
			.where("createdBy", "==", user.displayName)
			.orderBy("createdAt", "desc")
			.limit(LIMIT_PUBLICATIONS)
			.get()
			.then((response) => {
				if (response.docs.length > 0) {
					setPointer(response.docs[response.docs.length - 1]);
				} else {
					setLoading(false);
				}

				response.forEach((doc) => {
					const publication = doc.data();
					resultPublications.push({ id: doc.id, ...publication });
				});
				setPosts([...posts, ...resultPublications]);
			});
	};

	const renderItem = ({ item }) => <Card navigation={navigation} data={item} id={item.id} />;

	return (
		
		<LinearGradient colors={['#4ca4fc','#fa64ca']} >
			<ScrollView>
		<View style={styles.header}>
				
				<Ionicons name="arrow-back-sharp" size={30} color="white" onPress={navigation.goBack}/>
				<View style={{margin:90,marginBottom:0,marginTop:0}}>
				<Text style={{fontSize:20, fontWeight:'bold',color:'white'}}>Your Uploads</Text>
				</View>			
				</View>
		<View>
			{posts ? (
				
					<FlatList
					data={posts}
					style={{ marginBottom: 30 }}
					renderItem={renderItem}
					keyExtractor={(item) => item.id}
					// onEndReachedThreshold={0.5}
					// onEndReached={handleMore}
					ListFooterComponent={<FooterList loading={loading} />}
				/>
				

			) : (
				<Text>loading...</Text>
			)}
		</View>
		</ScrollView>
		</LinearGradient>
		
	);
}

function FooterList(props) {
	const { loading } = props;
	if (loading) {
		return (
			<View style={styles.loading}>
				<ActivityIndicator size="large" />
			</View>
		);
	} else {
		return (
			<View style={styles.loading}>
				<Text>No more posts found at the moment</Text>
			</View>
		);
	}
}


const styles = StyleSheet.create({
	loading: {
		marginTop: 10,
		marginBottom: 20,
		alignItems: "center",
	},
	header:{
		flexDirection:'row',
		paddingLeft:15,
		height:60,
		alignItems:'center',
		// backgroundColor:'#38c1bd'
	},
});
