import React, { useState, useContext } from "react";
import { Alert, Button, Dimensions,ScrollView, View,Text, TouchableOpacity,StyleSheet } from "react-native";
import { } from "react-native-gesture-handler";
import { Pagination } from "react-native-snap-carousel";
import Carousel from "../components/Publications/Carousel";
import Description from "../components/Publications/Description";
import { PRIMARY_COLOR } from "../utils/constants";
import { FirebaseContext } from "../context/firebase/FirebaseContext";
import { NavigationContainer, useNavigation } from "@react-navigation/native";
import { Ionicons } from '@expo/vector-icons';
import {LinearGradient} from 'expo-linear-gradient'

export default function Userpublication({route}) {

  const [activeSlide, setActiveSlide] = useState(0);
  const width = Dimensions.get("window").width
  const { user, firebase, db } = useContext(FirebaseContext)
  const navigation = useNavigation();
  const data = route.params.data;
	const id = route.params.id;
 
  const deletePost = () => {
	db.collection('posts').doc(id)
	.delete()
	Alert.alert('Post succesfully deleted')
	navigation.navigate("UserUploads")
	console.log(id)
  }
  const openTwoButtonAlert = () => {
	Alert.alert(
		'Delete Post',
		'Are you sure?',
		[
		  {text: 'Yes', onPress: () => deletePost()},
		  {text: 'No', onPress: () => Alert.alert('No item was removed'), style: 'cancel'},
		],
		{ 
		  cancelable: true 
		}
	  );
  }
	return (
		<View style={{backgroundColor:'white'}}>
		<ScrollView>
				<View style={styles.header}>
				
				<Ionicons name="arrow-back-sharp" size={30} color="white" onPress={navigation.goBack}/>
				<View style={{margin:110,marginBottom:0,marginTop:0}}>
				<Text style={{fontSize:20, fontWeight:'bold',color:'white'}}>Your Post</Text>
				</View>			
				</View>
			<Carousel
				images={data.images}
				width={width}
				height={250}
        setActiveSlide={setActiveSlide}
        
			/>
			<Pagination
				dotsLength={data.images.length}
        activeDotIndex={activeSlide}
        inactiveDotOpacity={1}
        dotStyle={{
          backgroundColor: '#4ca4fc',
		  height:13,
		  width:13,
		  borderRadius:12
        }}
        containerStyle={{
          paddingTop: 10,
          backgroundColor: 'white',
          paddingBottom: 10,
		  
        }}
			/>
      <View style={{margin:20,marginBottom:0,marginTop:40,}}>
		<Description data={data}/>
	  </View>
	  
	 
		</ScrollView>

		<View style={{flexDirection:'row',justifyContent:'center',marginTop:-30}}>

<TouchableOpacity onPress={()=> navigation.navigate("EditUpload", {id})} style={styles.edit}>
<Text style={{fontSize:20, fontWeight:'bold', color:'white'}}>Edit</Text>
</TouchableOpacity>

<TouchableOpacity onPress={()=> openTwoButtonAlert()} style={styles.delete}>
<Text style={{fontSize:20, fontWeight:'bold', color:'white'}}>Delete</Text>
</TouchableOpacity>
	
	</View>
		</View>
	);
}

const styles=StyleSheet.create({

	header:{
		flexDirection:'row',
		paddingLeft:15,
		height:60,
		alignItems:'center',
		backgroundColor:'#4ca4fc'
	},
	edit:{
		backgroundColor:'#4ca4fc',
		height:50,
		width:140, justifyContent:'center',
		alignItems:'center',
		borderRadius:15,
		margin:40,
		marginTop:-20,
		elevation:15,
		// marginBottom:40,
	},
	delete:{
		backgroundColor:'#4ca4fc',
		height:50,
		width:140, justifyContent:'center',
		alignItems:'center',
		borderRadius:15,
		margin:40,
		marginLeft:0,
		marginTop:-20,
		
		elevation:15
		
	}
})
