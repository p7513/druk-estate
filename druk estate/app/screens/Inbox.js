import React, {useEffect, useContext, useState,useLayoutEffect} from 'react'
import { View, Text, ScrollView, SafeAreaView, StyleSheet, TouchableOpacity, Image } from 'react-native'
import CustomListItem from '../components/CustomListItem';
import { FirebaseContext } from '../context/firebase/FirebaseContext';

export default function Inbox({navigation}) {
    const [chats, setChats] = useState([]);
    const { db } = useContext(FirebaseContext);
    useEffect(() => {
        const unsubscribe = db.collection('chats').onSnapshot((snapshot) =>
            setChats(
                snapshot.docs.map((doc) => ({
                    id: doc.id,
                    data: doc.data(),
                }))
            )
        )
        return unsubscribe;
    }, []);
    useLayoutEffect(() => {
        navigation.setOptions({
            title: "Messages",
            headerStyle: {backgroundColor: "#6462FA", height: 100},
            headerTitleStyle: {color: "white"},
            headerTitleColor: "white",
            })
    }, [navigation]);

    const enterChat = (id, chatName) =>{
        navigation.navigate("Chat", {
            id: id,
            chatName: chatName,
        })
    };
  return (
        <SafeAreaView>
            <ScrollView style={styles.container}>
               
                {chats.map(({ id, data: {chatName} }) => (
        
                    <CustomListItem   key={id} id={id} chatName={chatName} enterChat={enterChat}/>
                   
                ))}     
            </ScrollView>
        </SafeAreaView>
  )
}
const styles = StyleSheet.create({
    container: {
        height: '100%',
        borderColor: 'black',
    }
})
