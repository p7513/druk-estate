import React, { useContext, useState } from "react";
import uuid from "random-uuid-v4";
import { ScrollView, StyleSheet, TouchableOpacity, View, Text } from "react-native";
import FormContainer from "../components/FormContainer";
import FormPublish from "../components/Publish/FormPublish";
import ImageSlider from "../components/Publish/ImageSlider";
import { FirebaseContext } from "../context/firebase/FirebaseContext";
import Loading from "../components/Loading";
import Toast from "react-native-toast-message";
import { useNavigation } from "@react-navigation/native";
import {LinearGradient} from 'expo-linear-gradient'

export default function Edit({route}) {
	const { user, firebase, db } = useContext(FirebaseContext);
	const [title, setTitle] = useState("");
	const [description, setDescription] = useState("");
	const [imagesSelected, setImagesSelected] = useState([]);
	const [price, setPrice] = useState(1)
	const [location, setLocation] = useState(""); 
	const [uploading, setUploading] = useState(false);
	const navigation = useNavigation();
	const id = route.params.id;
	const handleSubmit = () => {
		if (!title || !description || !location || !price) {
			Toast.show({
				type: "error",
				text1: "Unable to post your listing, please fill in all the forms",
			});
		} else {
			setUploading(true);
			uploadImage().then((res) => {
				setUploading(false);
				db.collection("posts")
				.doc(id)
					.update({
						title: title,
						location: location,
						price: price,
						images: res,
						description: description,
						createdAt: new Date(),
						createdBy: user.displayName,
					})
					.then(() => {
						Toast.show({
							type: "success",
							text1: "Post successfully updated!",
						});
						clearForm();
						navigation.navigate('UserUploads');
					
					})
					.catch(() => {
						Toast.show({
							type: "error",
							text1: "Network error! Unable to upload your post",
							text2: ", please try again.",
						});
					});
			});
		}
	};

	const uploadImage = async () => {
		const imageBlob = [];

		await Promise.all(
			imagesSelected.map(async (image) => {
				const response = await fetch(image);
				const blob = await response.blob();

				const ref = firebase.storage().ref("posts").child(uuid());
				await ref.put(blob).then(async (res) => {
					await firebase
						.storage()
						.ref(`posts/${res.metadata.name}`)
						.getDownloadURL()
						.then((photoURL) => imageBlob.push(photoURL));
				});
			}),
		);

		return imageBlob;
	};

	const clearForm = () => {
		setTitle("");
		setDescription("");
		setImagesSelected([]);
		setPrice(0)
		setLocation("")
	};
	if (user === null) return <Loading isVisible={true} text={"loading..."} />;

	return (
		<>
		<LinearGradient colors={['#4ca4fc','#fa64ca']} style={{flex:1}}>
			<ScrollView>
			<View style={styles.header}>
				<Text style={{fontSize:20, fontWeight:'bold',color:'white'}}>Edit</Text>
						
				</View>
				<ImageSlider
					setImagesSelected={setImagesSelected}
					imagesSelected={imagesSelected}
				/>
				<FormContainer>
					<FormPublish
						description={description}
						title={title}
						setTitle={setTitle}
						setDescription={setDescription}
						setPrice={setPrice}
						price={price}
						location={location}
						setLocation ={setLocation}
					/>

						<TouchableOpacity onPress={handleSubmit}
				disabled={uploading} style={styles.btn}>
				<Text style={{fontSize:20,color:'white', fontWeight:'bold'}}>Update</Text>
				</TouchableOpacity>

				</FormContainer>
			
		
			</ScrollView>
		
			<Loading isVisible={uploading} text={"Updating"} />
			</LinearGradient>
		</>
	);
}
const styles=StyleSheet.create({
	header:{
	
		flexDirection:'row',
		paddingTop:30,
		height:60,
		alignItems:'center',
		justifyContent:'center'
		// backgroundColor:'#38c1bd'
	},
	btn:{
		justifyContent:'center',
		alignItems:'center',
		height:45,
		backgroundColor:'#8c76db',
		borderRadius:60,
		margin:80,
		marginTop:20,
		marginBottom:30,
		elevation:10

	},
})
