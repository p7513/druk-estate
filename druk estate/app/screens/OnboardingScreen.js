import React from 'react'
import {View, Text, Button, Image, StyleSheet, TouchableOpacity} from 'react-native'
import Onboarding from 'react-native-onboarding-swiper'

const Dots = ({selected}) => {
  let backgroundColor;

  backgroundColor = selected ? 'rgba(0, 0, 0, 0.8)' : 'rgba(0, 0, 0, 0.3)';

  return (
      <View 
          style={{
              width:6,
              height: 6,
              marginHorizontal: 3,
              backgroundColor
          }}
      />
  );
}

const Skip = ({...props}) => (
  <TouchableOpacity
      style={{marginHorizontal:10}}
      {...props}
  >
      <Text style={{fontSize:16}}>Skip</Text>
  </TouchableOpacity>
);

const Next = ({...props}) => (
  <TouchableOpacity
      style={{marginHorizontal:10}}
      {...props}
  >
      <Text style={{fontSize:16}}>Next</Text>
  </TouchableOpacity>
);

const Done = ({...props}) => (
  <TouchableOpacity
      style={{marginHorizontal:10}}
      {...props}
  >
      <Text style={{fontSize:16}}>Done</Text>
  </TouchableOpacity>
);

export default function OnboardingScreen({navigation}) {
  return (
    <Onboarding
        SkipButtonComponent={Skip}
        NextButtonComponent={Next}
        DoneButtonComponent={Done}
        DotComponent={Dots}
        onSkip={() => navigation.replace("Home")}
        onDone={() => navigation.replace("Home")}
        pages={[
          {
            backgroundColor: '#a6e4d0',
            image: <Image source={require('../../assets/onboarding-img1.png')} />,
            title: 'Kuzu Zangpo',
            subtitle: "Let's get started!",
          },
          {
            backgroundColor: '#fdeb93',
            image: <Image source={require('../../assets/onboarding-img2.png')} />,
            title: 'Find your dream house',
            subtitle: 'With variety of houses for rent, choose the right one!',
          },
          {
            backgroundColor: '#e9bcbe',
            image: <Image source={require('../../assets/onboarding-img3.png')} />,
            title: 'Live the dream',
            subtitle: "Perfect house to make your dream come true!",
          },
        ]}
      />
    );
};


const styles = StyleSheet.create({
  container: {
    flex: 1, 
    alignItems: 'center', 
    justifyContent: 'center'
  },
});