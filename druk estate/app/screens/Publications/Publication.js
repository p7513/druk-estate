import React, { useState, useContext } from "react";
import { Button,Text,View, Dimensions,ScrollView, TouchableOpacity,StyleSheet } from "react-native";
import { } from "react-native-gesture-handler";
import { Pagination } from "react-native-snap-carousel";
import Carousel from "../../components/Publications/Carousel";
import Description from "../../components/Publications/Description";
import { PRIMARY_COLOR } from "../../utils/constants";
import { FirebaseContext } from "../../context/firebase/FirebaseContext";
import { useNavigation } from "@react-navigation/native";
import {LinearGradient} from 'expo-linear-gradient'
import { Ionicons } from '@expo/vector-icons';
import UserGuest from "../Account/UserGuest";

export default function Publication({
	route: {
		params: { data},
	},
}) {
  const { db, isAuth, user } = useContext(FirebaseContext);
  const [activeSlide, setActiveSlide] = useState(0);
  const width = Dimensions.get("window").width;
  const navigation = useNavigation();

  const createChat = async () => {
	await db.collection('chats').add({
		chatName: data.createdBy,
		createdBy: user.uid
	}).then(() => {
		navigation.navigate('Inbox')
	}).catch((error) => alert(error.message) )
} 
	return (
		// <LinearGradient colors={['#4ca4fc','#fa64ca']} style={{flex:1}}>
		
			<View style={{flex:1}}>
				<ScrollView>
				<View style={styles.header}>
				
				<Ionicons name="arrow-back-sharp" size={30} color="white" onPress={navigation.goBack}/>
				<View style={{margin:125,marginBottom:0,marginTop:0}}>
				<Text style={{fontSize:20, fontWeight:'bold',color:'white'}}>Details</Text>
				</View>			
				</View>

			<Carousel
				images={data.images}
				width={width}
				height={250}
				
        setActiveSlide={setActiveSlide}
        
			/>
			<Pagination
				dotsLength={data.images.length}
        activeDotIndex={activeSlide}
        inactiveDotOpacity={1}
        dotStyle={{
          backgroundColor: '#4ca4fc',
		  height:12,
		  width:12,
		  borderRadius:10,
        }}
        containerStyle={{
          paddingTop: 7,
          
          paddingBottom: 7
        }}
			/>
			{/* <LinearGradient colors={['#b8e3d7','white']} style={styles.body}>	 */}
		
     <View  style={styles.body}>
	  <Description data={data} />
	  </View>	
		{/* <Button title="Contact Owner" onPress={()=>navigation.navigate('chat',{name:item.name,uid:item.uid,
                status :typeof(item.status) =="string"? item.status : item.status.toDate().toString()
            })}/> */}
			{/* <LinearGradient colors={['#05edab','#a4f5de']}  style={styles.btn}> */}
				</ScrollView>
			<TouchableOpacity onPress={() => isAuth ? createChat() : navigation.navigate('UserGuest')} style={styles.btn}>
				<Text style={{fontSize:20,color:'white', fontWeight:'bold'}}>Contact Owner</Text>
				</TouchableOpacity>
			{/* </LinearGradient> */}
			
		
			{/* </LinearGradient> */}
			
			</View>
		
		
	);
}

const styles=StyleSheet.create({
	header:{
		flexDirection:'row',
		paddingLeft:15,
		height:60,
		alignItems:'center',
		backgroundColor:'#4ca4fc'
	},
	btn:{
		justifyContent:'center',
		alignItems:'center',
		height:45,
		backgroundColor:'#4ca4fc',
		borderRadius:60,
		margin:60,
		marginTop:0,
		marginBottom:10,
		elevation:10

	},
	body:{
		// backgroundColor:'pink',
		padding:15,
		borderTopRightRadius:30,
		borderTopLeftRadius:30,
		margin:7,
		marginTop:20

	}
})