import React, { useCallback, useContext, useState } from "react";
import { StyleSheet } from "react-native";
import { View, Text, FlatList,ScrollView,  } from "react-native";
import Card from '../../components/Publications/Card'
import { FirebaseContext } from "../../context/firebase/FirebaseContext";
import { useFocusEffect, useNavigation } from "@react-navigation/native";
import {
	LIMIT_PUBLICATIONS,
} from "../../utils/constants";
import {LinearGradient} from 'expo-linear-gradient'
import { ActivityIndicator, Dimensions } from "react-native";
import { Ionicons ,FontAwesome5} from '@expo/vector-icons';
import { SearchBar } from 'react-native-elements';
import { TextInput } from "react-native-gesture-handler";



const {width,height}=Dimensions.get('window');
const cardWidth= width/2-30;

export default function Publications() {

	const { db, isAuth } = useContext(FirebaseContext);
	const [totalPublications, setTotalPublications] = useState(0);
	const [pointer, setPointer] = useState(null);
	const [posts, setPosts] = useState([]);
	const [loading, setLoading] = useState(false);
	const navigation = useNavigation();
	const [search, setSearch] = useState('');
 	const [filteredDataSource, setFilteredDataSource] = useState([]);

	useFocusEffect(
		useCallback(() => {
			db.collection("posts")
				.get()
				.then((snap) => {
					setTotalPublications(snap.size);
				});

			const resultPublications = [];
			const filteredDataSource = [];
			db.collection("posts")
				.orderBy("createdAt", "desc")
				.limit(LIMIT_PUBLICATIONS)
				.get()
				.then((snap) => {
					setPointer(snap.docs[snap.docs.length - 1]);
					snap.forEach((doc) => {
						resultPublications.push({ id: doc.id, ...doc.data() });
					});
					setPosts(resultPublications);
					setFilteredDataSource(resultPublications);
				});
				
		}, []),
	);

	const handleMore = () => {
		const resultPublications = [];
		if (posts.length < totalPublications) setLoading(true);
		db.collection("posts")
			.orderBy("createdAt", "desc")
			.startAfter(pointer.data().createdAt)
			.limit(LIMIT_PUBLICATIONS)
			.get()
			.then((response) => {
				if (response.docs.length > 0) {
					setPointer(response.docs[response.docs.length - 1]);
				} else {
					setLoading(false);
				}

				response.forEach((doc) => {
					const publication = doc.data();
					resultPublications.push({ id: doc.id, ...publication });
				});
				setPosts([...posts, ...resultPublications]);
			});
	};
	const searchFilterFunction = (text) => {
		if (text) {
		  
		  const newData = posts.filter(function (item) {
			const itemData = item.location
			  ? item.location.toUpperCase()
			  : ''.toUpperCase();
			const textData = text.toUpperCase();
			return itemData.indexOf(textData) > -1;
		  });
		  setFilteredDataSource(newData);
		  setSearch(text);
		} else {
		  setFilteredDataSource(posts);
		  setSearch(text);
		}
	  };

	const renderItem = ({ item }) => <Card navigation={navigation} data={item} />;

	return (
		<ScrollView>
		<LinearGradient colors={['#4ca4fc','#fa64ca','#4ca4fc',]} style={{flex:1}}>
		<View style={{flex:1,}}>
			{/* <ImageBackground source={require('../../../assets/home.webp')}/> */}
			<View style={styles.icon}>
			<Ionicons name="ios-chatbubble-ellipses" size={30} color="white" onPress={() => isAuth ? navigation.navigate('Inbox') : navigation.navigate('UserGuest')}/>
			</View>
			<View style={styles.head}>
				<Text style={{fontSize:26, fontWeight:'bold',color:'white'}}>Finding house made easy</Text>
			<Text style={{fontSize:10, fontWeight:'bold', color:'white'}}>Bringing your Dreams to Reality</Text>
			</View>
			{/* <SearchBar
          		round
          		searchIcon={{size: 30}}
          		onChangeText={(text) => searchFilterFunction(text)}
          		onClear={(text) => searchFilterFunction('')}
          		placeholder="Search"
          		value={search}
				style={{backgroundColor:'transparent'}}
				
        /> */}
		<View style={{flexDirection:'row', borderWidth:1,margin:25,marginTop:-10,marginBottom:20, padding:10, borderRadius:30,borderColor:'white',paddingLeft:20}}>
		<FontAwesome5 name="search-location" size={24} color="white" />
			<TextInput
			onChangeText={(text) => searchFilterFunction(text)}
			onClear={(text) => searchFilterFunction('')}
			placeholder="Search"
			placeholderTextColor='white'
			value={search}
			color='white'
			paddingLeft={15}
			/>
		</View>
		{/* <LinearGradient colors={['white','#d3e9f5','#9fd7f5']} style={styles.body}>	 */}
		<View style={styles.body}>
		
			{posts ? (
				<FlatList
				
					data={filteredDataSource}
					style={styles.post}
					renderItem={renderItem}
					keyExtractor={(item) => item.id}
					onEndReachedThreshold={0.5}
					// onEndReached={handleMore}
					ListFooterComponent={<FooterList loading ={loading} />}
					showsVerticalScrollIndicator={false}
                    numColumns={2}
				/>
			) : (
				<Text>loading...</Text>
			)}
			
		</View>
		{/* </LinearGradient>	 */}
		</View>
		</LinearGradient>
		</ScrollView>
	
	);
}

function FooterList(props) {
	const { loading } = props;
	if (loading) {
		return (
			<View style={styles.loading}>
				<ActivityIndicator size="large" />
			</View>
		);
	} else {
		return (
			<View style={styles.loading}>
				<Text>No more posts at the moment</Text>
			</View>
		);
	}
}



const styles = StyleSheet.create({
	loading: {
		marginTop: 10,
		marginBottom: 10,
		alignItems: "center",
	},
	icon:{
		marginLeft:width<380 ? 300:335,
		marginTop:40
	},
	head:{
		height:200,
		justifyContent:'center',
		alignItems:'center'
	},
	body:{
		flex:1,
		backgroundColor:'white',
		borderTopRightRadius:40,
		borderTopLeftRadius:40,
		padding:10,
		paddingBottom:0,
		marginTop:20
		
	},
	// post:{
	// 	flexDirection:'row',
	// 	backgroundColor:'red',
	// 	borderTopRightRadius:20,
	// 	borderTopLeftRadius:20
	// }
});
