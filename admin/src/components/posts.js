import React from 'react';
import {
    List,
    TextInput,
    SimpleForm,
    Edit,
    Create,
    ReferenceInput,
    SelectInput,
    Datagrid,
    ReferenceField,
    TextField,
    EmailField,
    EditButton,
    EditGuesser
} from 'react-admin';

export const PostList = props => (
    <List {...props}>
        <Datagrid rowClick="edit">
            <ReferenceField source="createdBy" reference="posts"><TextField source="posts"/></ReferenceField>
            <TextField source="createdBy"/>
            <TextField source="title"/>
            <TextField source="price"/>
            <TextField source="location"/>
            <TextField source="images"/>
            <EditButton/>
        </Datagrid>
    </List>
);

export const PostEdit = props => (
    <Edit {...props}>
        <SimpleForm>
        <ReferenceField source="ID" reference="posts"><TextField source="posts"/></ReferenceField>
            <TextField source="createdBy"/>
            <TextField source="title"/>
            <TextField source="price"/>
            <TextField source="location"/>
            <TextField source="images"/>
        </SimpleForm>
    </Edit>
);

export const PostCreate = props => (
    <Create {...props}>
        <SimpleForm>
        <ReferenceField source="createdBy" reference="posts"><TextField source="posts"/></ReferenceField>
            <TextInput source="createdBy"/>
            <TextInput source="title"/>
            <TextInput source="price"/>
            <TextInput source="location"/>
            <TextInput source="images"/>
        </SimpleForm>
    </Create>
);