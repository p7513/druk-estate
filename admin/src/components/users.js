import React from 'react';
import { List, Datagrid, TextField, EmailField } from 'react-admin';
import CustomEmailField from "./CustomEmailField";

export const UserList = props => (
    <List {...props}>
        <Datagrid rowClick="edit">
            <TextField source="displayName" />
            <CustomEmailField source="email" />
            <TextField source="phonenumber" sortable={false}/>
        </Datagrid>
    </List>
);