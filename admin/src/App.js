import React from 'react';
import {Admin, Resource} from 'react-admin';
import {UserList} from "./components/users";
import {PostCreate, PostEdit, PostList} from "./components/posts";
import {
    FirebaseDataProvider,
    FirebaseAuthProvider
} from 'react-admin-firebase';
import GroupIcon from '@material-ui/icons/Group';
import PostAddIcon from '@material-ui/icons/PostAdd';
// eslint-disable-next-line no-unused-vars

//connect the data provider to the REST endpoint

const config = {
    apiKey: "AIzaSyD2zkMZwNBkavhlSO81VjOAwQCBitzvnwk",
    authDomain: "druk-estate.firebaseapp.com",
    databaseURL: "https://druk-estate-default-rtdb.asia-southeast1.firebasedatabase.app",
    projectId: "druk-estate",
    storageBucket: "druk-estate.appspot.com",
    messagingSenderId: "34921178830",
    appId: "1:34921178830:web:e74a42bca3a47f73c6129f"
};

const options = {
    // Use a different root document to set your resource collections, by default it uses the root collections of firestore
     //rootRef: 'root-collection/some-doc',
// Your own, previously initialized firebase app instance
    //    eslint-disable-next-line no-undef
    //    app: firebaseAppInstance,
// Enable logging of react-admin-firebase
    logging: false,
// Resources to watch for realtime updates, will implicitly watch all resources by default, if not set.
//     watch: ['posts'],
// Resources you explicitly dont want realtime updates for
//     dontwatch: ['comments'],
}

const dataProvider = FirebaseDataProvider(config, options);
const firebaseAuthProvider = FirebaseAuthProvider(config,options)

function App() {
    return (
         <Admin dataProvider={dataProvider} authProvider = {firebaseAuthProvider}>
            <Resource name="users" list={UserList} icon={GroupIcon}/>
            <Resource name="posts" list={PostList} edit={PostEdit} create={PostCreate} icon={PostAddIcon}/>
            
        </Admin>
    );
}

export default App;
