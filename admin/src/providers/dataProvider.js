import * as React from 'react';
import { Admin, Resource } from 'react-admin';
import {UserList} from "../components/users";
import { PostList, PostShow, PostCreate, PostEdit } from "./posts";
import {
  FirebaseAuthProvider,
  FirebaseDataProvider,
  FirebaseRealTimeSaga
} from 'react-admin-firebase';

const config = {
    apiKey: "AIzaSyD2zkMZwNBkavhlSO81VjOAwQCBitzvnwk",
    authDomain: "druk-estate.firebaseapp.com",
    databaseURL: "https://druk-estate-default-rtdb.asia-southeast1.firebasedatabase.app",
    projectId: "druk-estate",
    storageBucket: "druk-estate.appspot.com",
    messagingSenderId: "34921178830",
    appId: "1:34921178830:web:e74a42bca3a47f73c6129f"
};

const options = {};

const dataProvider = FirebaseDataProvider(config, options);

      <Admin 
        dataProvider={dataProvider} 
      >
        <Resource name="posts" list={PostList} show={PostShow} create={PostCreate} edit={PostEdit}/>
        <Resource name="users" list={UserList} />
      </Admin>